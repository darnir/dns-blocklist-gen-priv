# DNS BlockList Generator

Generate DNS Zone Blocklists for use with unbound(8)

Unbound supports wildcard entries. This script filters out subdomains of
pre-existing domains in the list. As of this writing, when using the
StevenBlack hosts file as input, this script reduces the 103110 hosts entries
to 65010 unbound local zones.

## Usage

```
./gen-unbound-hosts.py [hosts-files...]
```

`hosts-file` can be either a local file or a URL. It will aggregate the hosts
from all input files and create one final deduplicated output DNS Zone file
for use with unbound.

The blocklists are generated once per day and the latest one can always be
found at the following link:

| DNS Server | Link                                                                                                                         |
|------------|------------------------------------------------------------------------------------------------------------------------------|
| Unbound    | [Block List](https://gitlab.com/darnir/dns-blocklist-generator/-/jobs/artifacts/main/raw/unbound-blocklist.conf?job=unbound) |

## License

GPLv3+
