#!/usr/bin/env python3

# Generate DNS Zone Blocklists for use with unbound(8)
#
# Unbound supports wildcard entries. This script filters out subdomains of
# pre-existing domains in the list. As of this writing, when using the
# StevenBlack hosts file as input, this script reduces the 103110 hosts entries
# to 65010 unbound local zones.

# Usage: ./gen-unbound-hosts.py [hosts-files...]
#
# hosts-file can be either a local file or a URL. It will aggregate the hosts
# from all input files and create one final deduplicated output DNS Zone file
# for use with unbound.

# Authors: Darshit Shah <git@darnir.net>
#          Savyasachee Jha <genghizkhan91@hawkradius.com>
#
# License: GPLv3+


import sys
import urllib.request

from pathlib import Path
from tld import is_tld


# Global variables
localtargets = {"0.0.0.0", "127.0.0.1", "::1"}
localhosts = {
    "localhost",
    "localhost.localdomain",
    "local",
    "broadcasthost",
    "ip6-localhost",
    "ip6-mcastprefix",
    "ip6-allnodes",
    "ip6-allrouters",
    "ip6-allhosts",
}


def get_hosts_file(url):
    return urllib.request.urlopen(url)


def parse(hosts_file):
    for line in hosts_file:
        line = line.decode("UTF-8").strip().split("#", 1)[0].strip()
        if not line:
            continue
        target, host = line.split()
        if target in localtargets and host not in (localhosts or localtargets):
            yield host


def add_to_blacklist(host, blacklist):
    if is_tld(host):
        return False
    for j in range(host.count("."), 0, -1):
        if host.split(".", j)[-1] in blacklist:
            return False
    return True


def gen_blocklist(input_hosts, blacklist):
    host_count = 0
    for host in sorted(parse(input_hosts), key=len):
        host_count += 1
        if add_to_blacklist(host, blacklist):
            blacklist.add(host)
    return (sorted(sorted(blacklist), key=len), host_count)


def write_blocklist_file(blist, fname):
    with open(fname, "w") as f:
        f.write('server:\n')
        for e in blist:
            f.write('local-zone: "' + e + '" always_nxdomain\n')


if __name__ == "__main__":
    blocklist = set()
    for arg in sys.argv[1:]:
        print(arg)
        if Path(arg).is_file():
            hosts_file = open(arg, "rb")
        else:
            try:
                hosts_file = get_hosts_file(arg)
            except ValueError:
                print("[{}]: Unknown path. Ignoring".format(arg))
                continue

        old_count = len(blocklist)
        blocklist, host_count = gen_blocklist(hosts_file, blocklist)
        print("[{}]:".format(arg))
        print(
            "\tReduced {} hosts to {} unbound local zones.".format(
                host_count, len(blocklist) - old_count
            )
        )

    write_blocklist_file(blocklist, "unbound-blocklist.conf")
